// ignore_for_file: public_member_api_docs, avoid_positional_boolean_parameters
@JS()
library firebase_node.interop.messaging;

import 'package:js/js.dart';

import '../../utils/es6_interop.dart';
import 'admin_interop.dart';
import 'app_interop.dart';

@JS('AndroidConfig')
@anonymous
abstract class AndroidConfigJsImpl {
  external factory AndroidConfigJsImpl({
    String collapseKey,
    dynamic /*Map<String,String> */ data,
    AndroidFcmOptions fcmOptions,
    AndroidNotificationJsImpl notification,
    String priority,
    String restrictedPackageName,
    num ttl,
  });

  external String get collapseKey;
  external set collapseKey(String v);

  external dynamic /*Map<String,String> */ get data;
  external set data(dynamic /*Map<String,String> */ v);

  external AndroidFcmOptions get fcmOptions;
  external set fcmOptions(AndroidFcmOptions v);

  external AndroidNotificationJsImpl get notification;
  external set notification(AndroidNotificationJsImpl v);

  external String get priority;
  external set priority(String v);

  external String get restrictedPackageName;
  external set restrictedPackageName(String v);

  external num get ttl;
  external set ttl(num v);
}

@JS()
@anonymous
abstract class AndroidFcmOptions {
  external factory AndroidFcmOptions({
    String analyticsLabel,
  });
  external String get analyticsLabel;
  external set analyticsLabel(String v);
}

@JS('AndroidNotification')
@anonymous
abstract class AndroidNotificationJsImpl {
  external factory AndroidNotificationJsImpl({
    String body,
    String bodyLocArgs,
    String bodyLocKey,
    String channelId,
    String clickAction,
    String color,
    bool defaultLightSettings,
    bool defaultSound,
    bool defaultVibrateTimings,
    DateTime eventTimestamp,
    String icon,
    String imageUrl,
    LightSettings lightSettings,
    bool localOnly,
    num notificationCount,
    String priority,
    String sound,
    bool sticky,
    String tag,
    String ticker,
    String title,
    String titleLocArgs,
    String titleLocKey,
    List /*num*/ vibrateTimingsMillis,
    String visibility,
  });
  external String get body;
  external set body(String v);

  external String get bodyLocArgs;
  external set bodyLocArgs(String v);

  external String get bodyLocKey;
  external set bodyLocKey(String v);

  external String get channelId;
  external set channelId(String v);

  external String get clickAction;
  external set clickAction(String v);

  external String get color;
  external set color(String v);

  external bool get defaultLightSettings;
  external set defaultLightSettings(bool v);

  external bool get defaultSound;
  external set defaultSound(bool v);

  external bool get defaultVibrateTimings;
  external set defaultVibrateTimings(bool v);

  external DateTime get eventTimestamp;
  external set eventTimestamp(DateTime v);

  external String get icon;
  external set icon(String v);

  external String get imageUrl;
  external set imageUrl(String v);

  external LightSettings get lightSettings;
  external set lightSettings(LightSettings v);

  external bool get localOnly;
  external set localOnly(bool v);

  external num get notificationCount;
  external set notificationCount(num v);

  external String get priority;
  external set priority(String v);

  external String get sound;
  external set sound(String v);

  external bool get sticky;
  external set sticky(bool v);

  external String get tag;
  external set tag(String v);

  external String get ticker;
  external set ticker(String v);

  external String get title;
  external set title(String v);

  external String get titleLocArgs;
  external set titleLocArgs(String v);

  external String get titleLocKey;
  external set titleLocKey(String v);

  external List /*num*/ get vibrateTimingsMillis;
  external set vibrateTimingsMillis(List /*num*/ v);

  external String get visibility;
  external set visibility(String v);
}

@JS('ApnsConfig')
@anonymous
abstract class ApnsConfigJsImpl {
  external factory ApnsConfigJsImpl({
    ApnsFcmOptions fcmOptions,
    dynamic /*Map<String,String>*/ headers,
    ApnsPayloadJsImpl payload,
  });

  external ApnsFcmOptions get fcmOptions;
  external set fcmOptions(ApnsFcmOptions v);

  external dynamic /*Map<String,String>*/ get headers;
  external set headers(dynamic /*Map<String,String>*/ v);

  external ApnsPayloadJsImpl get payload;
  external set payload(ApnsPayloadJsImpl v);
}

@JS()
@anonymous
abstract class ApnsFcmOptions {
  external factory ApnsFcmOptions({
    String analyticsLabel,
    String imageUrl,
  });

  external String get analyticsLabel;
  external set analyticsLabel(String v);

  external String get imageUrl;
  external set imageUrl(String v);
}

@JS('ApnsPayload')
@anonymous
abstract class ApnsPayloadJsImpl {
  external factory ApnsPayloadJsImpl({
    ApsJsImpl aps,
    dynamic /*Map<String,dynamic>*/ customData,
  });

  external ApsJsImpl get aps;
  external set aps(ApsJsImpl v);

  external dynamic /*Map<String,dynamic>*/ get customData;
  external set customData(dynamic /*Map<String,dynamic>*/ v);
}

@JS('ApsAlert')
@anonymous
abstract class ApsAlertJsImpl {
  external String get actionLocKey;
  external set actionLocKey(String v);

  external String get body;
  external set body(String v);

  external String get launchImage;
  external set launchImage(String v);

  external List/*<String>*/ get locArgs;
  external set locArgs(List/*<String>*/ v);

  external String get locKey;
  external set locKey(String v);

  external String get subtitle;
  external set subtitle(String v);

  external List/*<String>*/ get subtitleLocArgs;
  external set subtitleLocArgs(List/*<String>*/ v);

  external String get subtitleLocKey;
  external set subtitleLocKey(String v);

  external String get title;
  external set title(String v);

  external List/*<String>*/ get titleLocArgs;
  external set titleLocArgs(List/*<String>*/ v);

  external String get titleLocKey;
  external set titleLocKey(String v);
}

@JS('Aps')
@anonymous
abstract class ApsJsImpl {
  external dynamic /*String | ApsAlert*/ get alert;
  external set alert(dynamic /*String | ApsAlert*/ v);

  external num get badge;
  external set badge(num v);

  external String get category;
  external set category(String v);

  external bool get contentAvailable;
  external set contentAvailable(bool v);

  external dynamic /*Map<String,dynamic>*/ get customData;
  external set customData(dynamic /*Map<String,dynamic>*/ v);

  external bool get mutableContent;
  external set mutableContent(bool v);

  external dynamic /*String | CriticalSound*/ get sound;
  external set sound(dynamic /*String | CriticalSound*/ v);

  external String get threadId;
  external set threadId(String v);
}

@JS('BaseMessagee')
@anonymous
abstract class BaseMessageJsImpl {
  external AndroidConfigJsImpl get android;
  external set android(AndroidConfigJsImpl v);

  external ApnsConfigJsImpl get apns;
  external set apns(ApnsConfigJsImpl v);

  external dynamic /*Map<String,dynamic>*/ get data;
  external set data(dynamic /*Map<String,dynamic>*/ v);

  external FcmOptions get fcmOptions;
  external set fcmOptions(FcmOptions v);

  external MessagingNotification get notification;
  external set notification(MessagingNotification v);

  external WebpushConfigJsImpl get webpush;
  external set webpush(WebpushConfigJsImpl v);
}

@JS('BatchResponse')
@anonymous
abstract class BatchResponseJsImpl {
  external num get failureCount;
  external set failureCount(num v);

  external List/*<SendResponse>*/ get responses;
  external set responses(List/*<SendResponse>*/ v);

  external num get successCount;
  external set successCount(num v);
}

@JS('ConditionMessage')
@anonymous
abstract class ConditionMessageJsImpl extends BaseMessageJsImpl {
  external factory ConditionMessageJsImpl({
    AndroidConfigJsImpl android,
    ApnsConfigJsImpl apns,
    dynamic /*Map<String,dynamic>*/ data,
    FcmOptions fcmOptions,
    MessagingNotification notification,
    WebpushConfigJsImpl webpush,
    String condition,
  });
  external String get condition;
  external set condition(String v);
}

@JS()
@anonymous
abstract class CriticalSound {
  external bool get critical;
  external set critical(bool v);

  external String get name;
  external set name(String v);

  external num get volume;
  external set volume(num v);
}

@JS()
@anonymous
abstract class FcmOptions {
  external String get analyticsLabel;
  external set analyticsLabel(String v);
}

@JS()
@anonymous
abstract class LightSettings {
  external String get color;
  external set color(String v);

  external num get lightOffDurationMillis;
  external set lightOffDurationMillis(num v);

  external num get lightOnDurationMillis;
  external set lightOnDurationMillis(num v);
}

@JS()
@anonymous
abstract class MessagingConditionResponse {
  external num get messageId;
  external set messageId(num v);
}

@JS('MessagingDeviceGroupResponse')
@anonymous
abstract class MessagingDeviceGroupResponseJsImpl {
  external List/*<String>*/ get failedRegistrationTokens;
  external set failedRegistrationTokens(List/*<String>*/ v);

  external num get failureCount;
  external set failureCount(num v);

  external num get successCount;
  external set successCount(num v);
}

@JS()
@anonymous
abstract class MessagingDeviceResult {
  external String get canonicalRegistrationToken;
  external set canonicalRegistrationToken(String v);

  external FirebaseErrorJsImpl get error;
  external set error(FirebaseErrorJsImpl v);

  external String get messageId;
  external set messageId(String v);
}

@JS()
@anonymous
abstract class MessagingDevicesResponse {
  external num get canonicalRegistrationTokenCount;
  external set canonicalRegistrationTokenCount(num v);

  external num get failureCount;
  external set failureCount(num v);

  external MessagingDeviceResult get multicastId;
  external set multicastId(MessagingDeviceResult v);

  external num get results;
  external set results(num v);

  external num get successCount;
  external set successCount(num v);
}

@JS('Messaging')
@anonymous
abstract class MessagingJsImpl {
  external AppJsImpl get app;

  external PromiseJsImpl<String> send(
    dynamic /*TokenMessage | TopicMessage | ConditionMessage*/ message, [
    bool dryRun,
  ]);

  external PromiseJsImpl<BatchResponseJsImpl> sendAll(
    List /*TokenMessage | TopicMessage | ConditionMessage*/ message, [
    bool dryRun,
  ]);

  external PromiseJsImpl<BatchResponseJsImpl> sendMulticast(
    MulticastMessageJsImpl message, [
    bool dryRun,
  ]);

  external PromiseJsImpl<MessagingConditionResponse> sendToCondition(
    String condition,
    MessagingPayloadJsImpl payload, [
    MessagingOptions options,
  ]);

  external PromiseJsImpl<MessagingDevicesResponse> sendToDevice(
    dynamic /*String | List<String>*/ registrationToken,
    MessagingPayloadJsImpl payload, [
    MessagingOptions options,
  ]);

  external PromiseJsImpl<MessagingDeviceGroupResponseJsImpl> sendToDeviceGroup(
    String notificationKey,
    MessagingPayloadJsImpl payload, [
    MessagingOptions options,
  ]);

  external PromiseJsImpl<MessagingTopicResponse> sendToTopic(
    String topic,
    MessagingPayloadJsImpl payload, [
    MessagingOptions options,
  ]);

  external PromiseJsImpl<MessagingTopicManagementResponseJsImpl>
      subscribeToTopic(
    dynamic /*String | List<String>*/ registrationToken,
    String topic,
  );

  external PromiseJsImpl<MessagingTopicManagementResponseJsImpl>
      unsubscribeFromTopic(
    dynamic /*String | List<String>*/ registrationToken,
    String topic,
  );
}

@JS()
@anonymous
abstract class MessagingOptions {
  external factory MessagingOptions({
    String collapseKey,
    bool contentAvailable,
    bool dryRun,
    bool mutableContent,
    String priority,
    bool restrictedPackageName,
    num timeToLive,
  });

  external String get collapseKey;
  external set collapseKey(String v);

  external bool get contentAvailable;
  external set contentAvailable(bool v);

  external bool get dryRun;
  external set dryRun(bool v);

  external bool get mutableContent;
  external set mutableContent(bool v);

  external String get priority;
  external set priority(String v);

  external bool get restrictedPackageName;
  external set restrictedPackageName(bool v);

  external num get timeToLive;
  external set timeToLive(num v);
}

@JS('MessagingPayload')
@anonymous
abstract class MessagingPayloadJsImpl {
  external factory MessagingPayloadJsImpl({
    dynamic data,
    NotificationMessagePayload notification,
  });

  external dynamic /*Map<String,dynamic>*/ get data;
  external set data(dynamic /*Map<String,dynamic>*/ v);

  external NotificationMessagePayload get notification;
  external set notification(NotificationMessagePayload v);
}

@JS('MessagingTopicManagementResponse')
@anonymous
abstract class MessagingTopicManagementResponseJsImpl {
  external List /*FirebaseArrayIndexError*/ get errors;
  external set errors(List /*FirebaseArrayIndexError*/ v);

  external num get failureCount;
  external set failureCount(num v);

  external num get successCount;
  external set successCount(num v);
}

@JS()
@anonymous
abstract class MessagingTopicResponse {
  external num get messageId;
  external set messageId(num v);
}

@JS('MulticastMessage')
@anonymous
abstract class MulticastMessageJsImpl extends BaseMessageJsImpl {
  external factory MulticastMessageJsImpl({
    AndroidConfigJsImpl android,
    ApnsConfigJsImpl apns,
    dynamic /*Map<String,dynamic>*/ data,
    FcmOptions fcmOptions,
    MessagingNotification notification,
    WebpushConfigJsImpl webpush,
    List/*<String<*/ tokens,
  });

  external List/*<String<*/ get tokens;
  external set tokens(List/*<String<*/ v);
}

@JS()
@anonymous
abstract class MessagingNotification {
  external String get body;
  external set body(String v);

  external String get imageUrl;
  external set imageUrl(String v);

  external String get title;
  external set title(String v);
}

@JS()
@anonymous
abstract class NotificationMessagePayload {
  external String get badge;
  external set badge(String v);

  external String get body;
  external set body(String v);

  external String get bodyLocKey;
  external set bodyLocKey(String v);

  external String get clickAction;
  external set clickAction(String v);

  external String get color;
  external set color(String v);

  external String get icon;
  external set icon(String v);

  external String get bodyLocArgs;
  external set bodyLocArgs(String v);

  external String get sound;
  external set sound(String v);

  external String get tag;
  external set tag(String v);

  external String get title;
  external set title(String v);

  external String get titleLocArgs;
  external set titleLocArgs(String v);

  external String get titleLocKey;
  external set titleLocKey(String v);
}

@JS()
@anonymous
abstract class SendResponse {
  external FirebaseErrorJsImpl get error;
  external set error(FirebaseErrorJsImpl v);

  external String get messageId;
  external set messageId(String v);

  external bool get success;
  external set success(bool v);
}

@JS('TokenMessage')
@anonymous
abstract class TokenMessageJsImpl extends BaseMessageJsImpl {
  external factory TokenMessageJsImpl({
    AndroidConfigJsImpl android,
    ApnsConfigJsImpl apns,
    dynamic /*Map<String,dynamic>*/ data,
    FcmOptions fcmOptions,
    MessagingNotification notification,
    WebpushConfigJsImpl webpush,
    String token,
  });

  external String get token;
  external set token(String v);
}

@JS('TopicMessage')
@anonymous
abstract class TopicMessageJsImpl extends BaseMessageJsImpl {
  external factory TopicMessageJsImpl({
    AndroidConfigJsImpl android,
    ApnsConfigJsImpl apns,
    dynamic /*Map<String,dynamic>*/ data,
    FcmOptions fcmOptions,
    MessagingNotification notification,
    WebpushConfigJsImpl webpush,
    String topic,
  });

  external String get topic;
  external set topic(String v);
}

@JS()
@anonymous
abstract class WebPushActions {
  external String get action;
  external set action(String v);

  external String get icon;
  external set icon(String v);

  external String get title;
  external set title(String v);
}

@JS('WebpushConfig')
@anonymous
abstract class WebpushConfigJsImpl {
  external dynamic /*Map<String,String>*/ get data;
  external set data(dynamic /*Map<String,String>*/ v);

  external WebpushFcmOptions get fcmOptions;
  external set fcmOptions(WebpushFcmOptions v);

  external dynamic /*Map<String,String>*/ get headers;
  external set headers(dynamic /*Map<String,String>*/ v);

  external WebpushNotificationJsImpl get notification;
  external set notification(WebpushNotificationJsImpl v);
}

@JS()
@anonymous
abstract class WebpushFcmOptions {
  external String get link;
  external set link(String v);
}

@JS('WebpushNotification')
@anonymous
abstract class WebpushNotificationJsImpl {
  external List/*<WebpushAction>*/ get actions;
  external set actions(List/*<WebpushAction>*/ v);

  external String get badge;
  external set badge(String v);

  external String get body;
  external set body(String v);

  external dynamic /*Map<String,dynamic>*/ get data;
  external set data(dynamic /*Map<String,dynamic>*/ v);

  external String get dir;
  external set dir(String v);

  external String get icon;
  external set icon(String v);

  external String get image;
  external set image(String v);

  external String get lang;
  external set lang(String v);

  external bool get renotify;
  external set renotify(bool v);

  external bool get requireInteraction;
  external set requireInteraction(bool v);

  external bool get silent;
  external set silent(bool v);

  external String get tag;
  external set tag(String v);

  external num get timestamp;
  external set timestamp(num v);

  external String get title;
  external set title(String v);

  external dynamic /*num | List<num>*/ get vibrate;
  external set vibrate(dynamic /*num | List<num>*/ v);
}
