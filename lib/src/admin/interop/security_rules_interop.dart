// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.admin.security_rules;

import 'package:js/js.dart';
import 'package:meta/meta.dart';

import '../../utils/es6_interop.dart';
import 'app_interop.dart';

@JS()
@anonymous
abstract class Ruleset {
  external String get createTime;
  external set createTime(String v);

  external String get name;
  external set name(String v);

  external List<RulesFile> get source;
  external set source(List<RulesFile> v);
}

@JS()
@anonymous
abstract class RulesetMetadata {
  external factory RulesetMetadata({
    String createTime,
    String name,
  });

  external String get createTime;
  external set createTime(String v);

  external String get name;
  external set name(String v);
}

@JS()
@anonymous
abstract class RulesetMetadataList {
  external factory RulesetMetadataList({
    String nextPageToken,
    @required List<RulesetMetadata> rulesets,
  });

  external String get nextPageToken;
  external set nextPageToken(String v);

  external List<RulesetMetadata> get rulesets;
  external set rulesets(List<RulesetMetadata> v);
}

@JS()
@anonymous
abstract class RulesFile {
  external factory RulesFile({
    String content,
    String name,
  });

  external String get content;
  external set content(String v);

  external String get name;
  external set name(String v);
}

@JS('SecurityRules')
@anonymous
abstract class SecurityRulesJsImpl {
  external AppJsImpl get app;
  external PromiseJsImpl<Ruleset> createRuleset(RulesFile file);
  external RulesFile createRulesFileFromSource(String name, String source);
  external PromiseJsImpl<void> deleteRuleset(String name);
  external PromiseJsImpl<Ruleset> getFirestoreRuleset();
  external PromiseJsImpl<Ruleset> getRuleset(String name);
  external PromiseJsImpl<Ruleset> getStorageRuleset([String bucket]);
  external PromiseJsImpl<RulesetMetadataList> listRulesetMetadata(
      [num pageSize, String nextPageToken]);
  external PromiseJsImpl<void> releaseFirestoreRuleset(
      /*String | RulesetMetadata */ dynamic ruleset);
  external PromiseJsImpl<Ruleset> releaseFirestoreRulesetFromSource(
      String source);
  external PromiseJsImpl<void> releaseStorageRuleset(
    /*String | RulesetMetadata */ dynamic ruleset, [
    String bucket,
  ]);
  external PromiseJsImpl<void> releaseStorageRulesetFromSource(
    /*String | RulesetMetadata */ dynamic ruleset, [
    String bucket,
  ]);
}
