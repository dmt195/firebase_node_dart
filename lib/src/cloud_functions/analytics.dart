import 'package:js/js.dart';

import '../utils/js.dart';
import '../utils/utils.dart';
import 'functions.dart';
import 'interop/analytics_interop.dart' as interop;

export 'interop/analytics_interop.dart'
    show AnalyticsAppInfo, DeviceInfo, ExportBundleInfo, GeoInfo;

///Interface representing a Firebase Analytics event that was logged for a
///specific user.
class AnalyticsEvent extends JsObjectWrapper<interop.AnalyticsEventJsImpl> {
  AnalyticsEvent._fromJsObject(interop.AnalyticsEventJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///UTC client time when the event happened.
  String get logTime => jsObject.logTime;

  ///The name of the event.
  String get name => jsObject.name;

  ///A map of parameters and their values associated with the event.
  ///Note: Values in this map are cast to the most appropriate type. Due to the
  ///nature of JavaScript's number handling, this might entail a loss of
  ///precision in cases of very large integers.
  Map<String, dynamic> get params => dartify(jsObject.params);

  ///UTC client time when the previous event happened.
  String get previousLogTime => jsObject.previousLogTime;

  ///The date on which the event.was logged. (YYYYMMDD format in the registered
  ///timezone of your app).
  String get reportingDate => jsObject.reportingDate;

  ///User-related dimensions.
  UserDimensions get user => UserDimensions._fromJsObject(jsObject.user);

  ///Value parameter in USD.
  num get valueInUSD => jsObject.valueInUSD;

  static AnalyticsEvent _getInstance(interop.AnalyticsEventJsImpl jsObject) =>
      AnalyticsEvent._fromJsObject(jsObject);
}

///The Firebase Analytics event builder interface.
class AnalyticsEventBuilder
    extends JsObjectWrapper<interop.AnalyticsEventBuilderJsImpl> {
  AnalyticsEventBuilder._fromJsObject(
      interop.AnalyticsEventBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Event handler that fires every time a Firebase Analytics event occurs.
  dynamic onLog(CloudFunction<AnalyticsEvent> handler) {
    return jsObject.onLog(allowInterop((eventJs, contextJs) {
      return cloudFunction(
        AnalyticsEvent._getInstance(eventJs),
        EventContext.fromJsObject(contextJs),
        handler,
      );
    }));
  }
}

///The Firebase Analytics functions interface.
class AnalyticsFunctionsBuilder
    extends JsObjectWrapper<interop.AnalyticsFunctionsBuilderJsImpl> {
  ///The Firebase Analytics functions interface.
  AnalyticsFunctionsBuilder.fromJsObject(
      interop.AnalyticsFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Registers a function to handle analytics events.
  AnalyticsEventBuilder event(String analyticsEventType) =>
      AnalyticsEventBuilder._fromJsObject(jsObject.event(analyticsEventType));
}

///Interface representing the user who triggered the events.
class UserDimensions extends JsObjectWrapper<interop.UserDimensionsJsImpl> {
  UserDimensions._fromJsObject(interop.UserDimensionsJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///App information.
  interop.AnalyticsAppInfo get appInfo => jsObject.appInfo;

  ///Information regarding the bundle in which these events were uploaded.
  interop.ExportBundleInfo get bundleInfo => jsObject.bundleInfo;

  ///Device information.
  interop.DeviceInfo get deviceInfo => jsObject.deviceInfo;

  ///The time (in UTC) at which the user first opened the app.
  String get firstOpenTime => jsObject.firstOpenTime;

  ///User's geographic information.
  interop.GeoInfo get geoInfo => jsObject.geoInfo;

  ///The user ID set via the setUserId API. Android iOS
  String get userId => jsObject.userId;

  ///A map of user properties set with the `setUserProperty` API.
  ///
  ///All values are UserPropertyValue objects.
  Map<String, UserPropertyValue> get userProperties {
    final map = dartify(jsObject.userProperties);
    if (map == null) return null;

    return (map as Map<String, dynamic>).cast<String, Map>().map(
        (k, v) => MapEntry(k, UserPropertyValue(v['setTime'], v['value'])));
  }
}

///Predefined or custom properties stored on the client side.
class UserPropertyValue {
  ///UTC client time when the user property was last set.
  final String setTime;

  ///The last set value of a user property.
  final String value;

  ///Predefined or custom properties stored on the client side.
  const UserPropertyValue(this.setTime, this.value);
}
