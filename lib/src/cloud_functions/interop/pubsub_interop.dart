// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.pubsub;

import 'package:js/js.dart';

import 'configuration_interop.dart';
import 'functions_interop.dart';

@JS()
@anonymous
abstract class MessageJsImpl {
  ///User-defined attributes published with the message, if any.
  external dynamic get attributes;

  ///The data payload of this message object as a base64-encoded string.
  external String get data;

  ///The JSON data payload of this message object, if any.
  external dynamic get json;
}

@JS()
@anonymous
abstract class PubsubFunctionsBuilderJsImpl {
  external ScheduleBuilderJsImpl schedule(String schedule);

  ///Select Cloud Pub/Sub topic to listen to.
  ///Name of Pub/Sub topic, must belong to the same project as the function.
  external TopicBuilderJsImpl topic(String topic);
}

@JS()
@anonymous
abstract class ScheduleBuilderJsImpl {
  external dynamic onRun(Function(EventContextJsImpl) handler);
  external ScheduleBuilderJsImpl retryConfig(ScheduleRetryConfig config);
  external ScheduleBuilderJsImpl timezone(String timezone);
}

@JS()
@anonymous
abstract class TopicBuilderJsImpl {
  ///Event handler that fires every time a Cloud Pub/Sub message is published.
  external dynamic onPublish(JsHandler<MessageJsImpl> handler);
}
