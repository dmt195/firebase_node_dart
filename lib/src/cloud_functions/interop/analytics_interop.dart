// ignore_for_file: public_member_api_docs
@JS()
library firebase_node.functions.interop.analytics;

import 'package:js/js.dart';

import 'functions_interop.dart';

@JS()
@anonymous
abstract class AnalyticsEventBuilderJsImpl {
  ///Event handler that fires every time a Firebase Analytics event occurs.
  external dynamic onLog(JsHandler<AnalyticsEventJsImpl> handler);
}

@JS()
@anonymous
abstract class AnalyticsEventJsImpl {
  ///UTC client time when the event happened.
  external String get logTime;

  ///The name of the event.
  external String get name;

  ///A map of parameters and their values associated with the event.
  ///Note: Values in this map are cast to the most appropriate type. Due to the
  ///nature of JavaScript's number handling, this might entail a loss of
  ///precision in cases of very large integers.
  external dynamic /*Map<String,dynamic> */ get params;

  ///UTC client time when the previous event happened.
  external String get previousLogTime;

  ///The date on which the event.was logged. (YYYYMMDD format in the registered
  ///timezone of your app).
  external String get reportingDate;

  ///User-related dimensions.
  external UserDimensionsJsImpl get user;

  ///Value parameter in USD.
  external num get valueInUSD;
}

@JS()
@anonymous
abstract class AnalyticsFunctionsBuilderJsImpl {
  ///Select analytics events to listen to for events.
  external AnalyticsEventBuilderJsImpl event(String analyticsEventType);
}

@JS('AppInfo')
@anonymous
abstract class AnalyticsAppInfo {
  ///Unique application identifier within an app store.
  external String get appId;

  ///Unique ID for this instance of the app.
  ///
  ///Example: "71683BF9FA3B4B0D9535A1F05188BAF3".
  external String get appInstanceId;

  ///The app platform.
  ///
  ///Examples: "ANDROID", "IOS".
  external String get appPlatform;

  ///The identifier of the store that installed the app.
  ///
  ///Examples: "com.sec.android.app.samsungapps", "com.amazon.venezia", "com.
  ///nokia.nstore".
  external String get appStore;

  ///The app's version name.
  ///
  ///Examples: "1.0", "4.3.1.1.213361", "2.3 (1824253)", "v1.8b22p6".
  external String get appVersion;
}

@JS()
@anonymous
abstract class DeviceInfo {
  ///Device category.
  ///
  ///Examples: "tablet" or "mobile".
  external String get deviceCategory;

  ///Vendor specific device identifier. This is IDFV on iOS. Not used for
  ///Android.
  ///
  ///Example: '599F9C00-92DC-4B5C-9464-7971F01F8370'
  external String get deviceId;

  ///Device model, as read from the OS.
  ///
  ///Example: "iPhone9,1"
  external String get deviceModel;

  ///The time zone of the device when data was uploaded, as seconds skew from
  ///UTC. Use this to calculate the device's local time for EventContext.
  ///timestamp.
  external num get deviceTimeZoneOffsetSeconds;

  ///The device's Limit Ad Tracking setting. When true, you cannot use
  ///resettableDeviceId for remarketing, demographics or influencing ads
  ///serving behaviour. However, you can use resettableDeviceId for conversion
  ///tracking and campaign attribution.
  external bool get limitedAdTracking;

  ///Device brand name.
  ///
  ///Examples: "Samsung", "HTC"
  external String get mobileBrandName;

  ///Device marketing name.
  ///
  ///Example: "Galaxy S4 Mini"
  external String get mobileMarketingName;

  ///Device OS version when data capture ended.
  ///
  ///Example: "4.4.2"
  external String get mobileModelName;

  ///Device OS version when data capture ended.
  ///
  ///Example: "4.4.2"
  external String get platformVersion;

  ///The type of the resettable_device_id is IDFA on iOS (when available) and
  ///AdId on Android.
  ///
  ///Example: "71683BF9-FA3B-4B0D-9535-A1F05188BAF3"
  external String get resettableDeviceId;

  ///The user language in language-country format, where language is an ISO 639
  ///value and country is an ISO 3166 value.
  ///
  ///Examples: "en-us", "en-za", "zh-tw", "jp"
  external String get userDefaultLanguage;
}

@JS()
@anonymous
abstract class ExportBundleInfo {
  ///Monotonically increasing index for each bundle set by the Analytics SDK.
  external num get bundleSequenceId;

  ///Timestamp offset (in milliseconds) between collection time and upload time.
  external num get serverTimestampOffset;
}

@JS()
@anonymous
abstract class GeoInfo {
  ///The geographic city.
  ///
  ///Example: "Sao Paulo".
  external String get city;

  ///The geographic continent.
  ///
  ///Example: "South America".
  external String get continent;

  ///The geographic country.
  ///
  ///Example: "Brazil".
  external String get country;

  ///The geographic region.
  ///
  ///Example: "State of Sao Paulo".
  external String get region;
}

@JS()
@anonymous
abstract class UserDimensionsJsImpl {
  ///App information.
  external AnalyticsAppInfo get appInfo;

  ///Information regarding the bundle in which these events were uploaded.
  external ExportBundleInfo get bundleInfo;

  ///Device information.
  external DeviceInfo get deviceInfo;

  ///The time (in UTC) at which the user first opened the app.
  external String get firstOpenTime;

  ///User's geographic information.
  external GeoInfo get geoInfo;

  ///The user ID set via the setUserId API. Android iOS
  external String get userId;

  ///A map of user properties set with the `setUserProperty` API.
  ///
  ///All values are UserPropertyValue objects.
  external dynamic /*Map<String,dynamic> */ get userProperties;
}
