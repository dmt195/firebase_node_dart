import 'package:js/js.dart';

import '../../admin.dart';
import '../utils/js.dart';
import 'functions.dart';
import 'interop/database_interop.dart' as interop;

///Contruct database functions
class DatabaseFunctionsBuilder
    extends JsObjectWrapper<interop.DatabaseFunctionsBuilderJsImpl> {
  ///Contruct database functions
  DatabaseFunctionsBuilder.fromJsObject(
      interop.DatabaseFunctionsBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Selects a database instance that will trigger the function. If omitted,
  ///will pick the default database for your project.
  InstanceBuilder instance(String instance) =>
      InstanceBuilder.getInstance(jsObject.instance(instance));

  ///Select Firebase Realtime Database Reference to listen to.
  RefBuilder ref(String path) => RefBuilder.getInstance(jsObject.ref(path));
}

///The Firebase Realtime Database instance builder interface.
class InstanceBuilder extends JsObjectWrapper<interop.InstanceBuilderJsImpl> {
  static final _expando = Expando<InstanceBuilder>();

  ///The Firebase Realtime Database instance builder interface.
  static InstanceBuilder getInstance(interop.InstanceBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= InstanceBuilder._fromJsObject(jsObject);
  }

  InstanceBuilder._fromJsObject(interop.InstanceBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Firebase Realtime Database reference builder interface.
  RefBuilder ref(String path) => RefBuilder.getInstance(jsObject.ref(path));
}

///The Firebase Realtime Database reference builder interface.
///
///Access via functions.database.ref().
class RefBuilder extends JsObjectWrapper<interop.RefBuilderJsImpl> {
  static final _expando = Expando<RefBuilder>();

  ///The Firebase Realtime Database reference builder interface.
  ///
  ///Access via functions.database.ref().
  static RefBuilder getInstance(interop.RefBuilderJsImpl jsObject) {
    if (jsObject == null) return null;
    return _expando[jsObject] ??= RefBuilder._fromJsObject(jsObject);
  }

  RefBuilder._fromJsObject(interop.RefBuilderJsImpl jsObject)
      : super.fromJsObject(jsObject);

  ///Event handler that fires every time new data is created in Firebase
  ///Realtime Database.
  dynamic onCreate(CloudFunction<DataSnapshot> handler) {
    return jsObject.onCreate(allowInterop((dataJs, contextJs) {
      final data = DataSnapshot.getInstance(dataJs);
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }

  ///Event handler that fires every time data is deleted from Firebase Realtime
  ///Database.
  dynamic onDelete(CloudFunction<DataSnapshot> handler) {
    return jsObject.onDelete(allowInterop((dataJs, contextJs) {
      final data = DataSnapshot.getInstance(dataJs);
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }

  ///Event handler that fires every time data is updated in Firebase Realtime
  ///Database.
  dynamic onUpdate(CloudFunction<Change<DataSnapshot>> handler) {
    return jsObject.onUpdate(allowInterop((dataJs, contextJs) {
      final data = Change(
        after: DataSnapshot.getInstance(dataJs.after),
        before: DataSnapshot.getInstance(dataJs.before),
      );
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }

  ///Event handler that fires every time a Firebase Realtime Database write of
  ///any kind (creation, update, or delete) occurs.
  dynamic onWrite(CloudFunction<Change<DataSnapshot>> handler) {
    return jsObject.onWrite(allowInterop((dataJs, contextJs) {
      final data = Change(
        after: DataSnapshot.getInstance(dataJs.after),
        before: DataSnapshot.getInstance(dataJs.before),
      );
      final context = EventContext.fromJsObject(contextJs);
      return cloudFunction(data, context, handler);
    }));
  }
}
