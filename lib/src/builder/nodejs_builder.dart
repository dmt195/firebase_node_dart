import 'dart:async';

import 'package:analyzer/dart/analysis/utilities.dart';
import 'package:analyzer/dart/ast/ast.dart';
import 'package:build/build.dart';

import 'dart2js_bootstrap.dart';

/// Returns whether or not [dartId] is an app entrypoint (basically, whether
/// or not it has a `main` function).
Future<bool> _isAppEntryPoint(AssetId dartId, AssetReader reader) async {
  assert(dartId.extension == '.dart');
  // Skip reporting errors here, dartdevc will report them later with nicer
  // formatting.
  // ignore: deprecated_member_use
  final parsed = parseString(
    content: await reader.readAsString(dartId),
    throwIfDiagnostics: false,
  );
  // Allow two or fewer arguments so that entrypoints intended for use with
  // [spawnUri] get counted.
  //
  return parsed.unit.declarations.any((node) =>
      node is FunctionDeclaration &&
      node.name.name == 'main' &&
      node.functionExpression.parameters.parameters.length <= 2);
}

class NodeJsBuilder implements Builder {
  final Map nodeModules;
  final List<String> dart2JsArgs;

  const NodeJsBuilder([
    this.dart2JsArgs,
    this.nodeModules,
  ]);
  // ]) : super(webCompiler, dart2JsArgs: dart2JsArgs);

  factory NodeJsBuilder.fromOptions(BuilderOptions options) {
    final dart2JsConfig = options.config['dart2js_args'] as List;
    final dart2JsArgs = dart2JsConfig?.cast<String>() ?? <String>[];
    final nodeModules = options.config['node_modules'] as Map;
    return NodeJsBuilder(dart2JsArgs, nodeModules);
  }

  @override
  Future<void> build(BuildStep buildStep) async {
    var dartEntrypointId = buildStep.inputId;
    var isAppEntrypoint = await _isAppEntryPoint(dartEntrypointId, buildStep);
    if (!isAppEntrypoint) return;
    await bootstrapDart2Js(
      buildStep,
      dart2JsArgs,
      nodeModules,
      skipPlatformCheck: true,
    );
  }

  @override
  final Map<String, List<String>> buildExtensions = const {
    '.dart': [
      ddcBootstrapExtension,
      jsEntrypointExtension,
      jsEntrypointSourceMapExtension,
      jsEntrypointArchiveExtension,
      digestsEntrypointExtension,
      preambleEntrypointExtension,
    ],
  };
}
